-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 03 Jan 2018 pada 09.51
-- Versi Server: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pab_d3u2017`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('c31565n8166802q0aceu5afdl7t48g7r', '::1', 1514947820, 0x5f5f63695f6c6173745f726567656e65726174657c693a313531343934373832303b);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `fnip` varchar(6) NOT NULL,
  `fnama` varchar(50) NOT NULL,
  `fpasswd` varchar(50) NOT NULL,
  `frole` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_admin`
--

INSERT INTO `tbl_admin` (`fnip`, `fnama`, `fpasswd`, `frole`) VALUES
('060015', 'Ferdiansyah', '12345', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_krs`
--

CREATE TABLE `tbl_krs` (
  `fthn_ajar` varchar(8) NOT NULL,
  `fsmt` char(1) NOT NULL,
  `fnim_mhs` varchar(10) NOT NULL,
  `fkd_matkul` varchar(6) NOT NULL,
  `fkelompok` char(2) NOT NULL,
  `ftlg_log` datetime NOT NULL,
  `fuserlog` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_matkul`
--

CREATE TABLE `tbl_matkul` (
  `fkd_matkul` varchar(6) NOT NULL,
  `fnm_matkul` varchar(50) DEFAULT NULL,
  `fjml_sks` int(2) NOT NULL,
  `fjenis_matkul` varchar(7) DEFAULT NULL,
  `fsingkatan_matkul` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tbl_matkul`
--

INSERT INTO `tbl_matkul` (`fkd_matkul`, `fnm_matkul`, `fjml_sks`, `fjenis_matkul`, `fsingkatan_matkul`) VALUES
('B4001', 'Bahasa Indonesia', 2, 'Inti', 'B. Indo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_mhs`
--

CREATE TABLE `tbl_mhs` (
  `fnim_mhs` varchar(10) NOT NULL,
  `fnama_mhs` varchar(50) NOT NULL,
  `fktp_mhs` varchar(25) NOT NULL,
  `fagama_mhs` varchar(10) NOT NULL,
  `fjenkel_mhs` char(1) NOT NULL,
  `ftgllahir_mhs` date NOT NULL,
  `fpasswd_mhs` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`fnip`);

--
-- Indexes for table `tbl_krs`
--
ALTER TABLE `tbl_krs`
  ADD PRIMARY KEY (`fthn_ajar`,`fsmt`);

--
-- Indexes for table `tbl_matkul`
--
ALTER TABLE `tbl_matkul`
  ADD PRIMARY KEY (`fkd_matkul`);

--
-- Indexes for table `tbl_mhs`
--
ALTER TABLE `tbl_mhs`
  ADD PRIMARY KEY (`fnim_mhs`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
