<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Krs_admin extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(['date', 'form']);
    }

    public function index()
    {
		// query_builder join
		/*
		$this->db->select('*')
				 ->from('tbl_krs')
				 ->join('tbl_mhs', 'tbl_mhs.fnim_mhs = tbl_krs.fnim_mhs')
				 ->join('tbl_matkul', 'tbl_matkul.fkd_matkul = tbl_krs.fkd_matkul');
		*/

		$get = $this->db->get('tbl_krs');

        $data['dt_krs'] = $get->result_array();

        $data['_content'] = 'v_krs/krs_view';

        $this->load->view('themes/main', $data);
    }

    public function tambah()
    {
        $data['_content'] = 'v_krs/add_krs_view';
        $data['fnim']     = $this->db->get('tbl_mhs')->result_array();
        $data['fmatkul']  = $this->db->get('tbl_matkul')->result_array();

        $this->load->view('themes/main', $data);
        
    }

    public function simpan($id = NULL)
	{
		if ($this->input->post('btn_simpan'))
		{
			$time_mysql = "%Y-%m-%d %H:%i:%s";
			$data = [
				'fthn_ajar'  => $this->input->post('txt_thn_ajar'),
				'fsmt'       => $this->input->post('txt_smt'),
				'fnim_mhs'   => $this->input->post('txt_nim'),
				'fkd_matkul' => $this->input->post('txt_matkul'),
				'fkelompok'  => $this->input->post('txt_kelompok'),
				'ftlg_log'   => mdate($time_mysql, time(date('Y-m-d H:i:s'))),
				'fuserlog'   => $this->session->userdata('user_id')
			];
			
			$this->db->insert('tbl_krs', $data);

			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data berhasil disimpan !!!</div>');
			}
			redirect('krs_admin');
		} 
		elseif ($this->input->post('btn_update')) 
		{
			$fkd_matkul = $this->input->post('txt_kd_krs_edit');
			$data = [
				'fthn_ajar'  => $this->input->post('txt_thn_ajar'),
				'fsmt'       => $this->input->post('txt_smt'),
				'fnim_mhs'   => $this->input->post('txt_nim'),
				'fkd_matkul' => $this->input->post('txt_matkul'),
				'fkelompok'  => $this->input->post('txt_kelompok'),
				'ftlg_log'   => mdate($time_mysql, time(date('Y-m-d H:i:s'))),
				'fuserlog'   => $this->session->userdata('user_id')
			];
			
			$this->db->update('tbl_krs', $data, ['fnim_mhs'=>$fnim, 'fkd_matkul' => $fkd_matkul]);

			if ($this->db->affected_rows() != 1)
			{
			    $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal diubah !!!</div>');
			}
			else
			{
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $fkd_matkul . ' berhasil diubah !!!</div>');
			}

			redirect('matkul');
		}
	}

	public function ubah($fnim, $fkd_matkul)
	{
		$query = $this->db->get_where('tbl_krs', ['fnim_mhs'=>$fnim, 'fkd_matkul'=>$fkd_matkul]);
		
		if ($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				$this->data['data_krs_edit'] = $row;
			}
		}
		else
		{
			$this->data['data_krs_edit'] = NULL;
		}
		$this->data['_content'] = 'v_krs/ubah_krs_view';

		$this->load->view('themes/main', $this->data);
	}

	public function hapus($fnim, $fkd_matkul)
	{
		$delete = $this->db->delete('tbl_krs', ['fnim_mhs'=>$fnim, 'fkd_matkul'=>$fkd_matkul]);
		if ($delete) 
		{
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data berhasil dihapus !!!</div>');
		}
		redirect('krs_admin');
	}

}

/* End of file Krs_admin.php */