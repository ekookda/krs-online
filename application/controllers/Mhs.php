<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Project Aplikasi
 * Kartu Rencana Studi 
 */
class Mhs extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	}

	public function index()
	{
		$data = [
			'dt_mhs' => $this->db->get('tbl_mhs')->result_array(),
			'_content' => 'v_mhs/mhs_view'
		];

		$this->load->view('themes/main', $data);
	}
	
	public function tambah()
	{
		$this->data['_content'] = 'v_mhs/add_mhs_view';

		$this->load->view('themes/main', $this->data);
	}

	public function simpan($id = NULL)
	{
		if ($this->input->post('btn_simpan'))
		{
			$data = [
				'fnim_mhs' => $this->input->post('txt_nim_mhs'),
				'fnama_mhs' => $this->input->post('txt_nama_mhs'),
				'fktp_mhs' => $this->input->post('txt_no_ktp'),
				'fagama_mhs' => $this->input->post('txt_agama'),
				'fjenkel_mhs' => $this->input->post('jenkel'),
				'ftgllahir_mhs' => $this->input->post('txt_tgl_lahir'),
				'fpasswd_mhs' => $this->input->post('txt_password')
			];
			
			$this->db->insert('tbl_mhs', $data);

			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data '. $data['txt_nim_mhs']. ' berhasil disimpan !!!</div>');
			}

			redirect('mhs');
		} 
		elseif ($this->input->post('btn_update')) 
		{
			$fnim_mhs = $this->input->post('txt_nim_mhs_edit');
			$data = [
				'fnim_mhs' => $this->input->post('txt_nim_mhs'),
				'fnama_mhs' => $this->input->post('txt_nama_mhs'),
				'fktp_mhs' => $this->input->post('txt_no_ktp'),
				'fagama_mhs' => $this->input->post('txt_agama'),
				'fjenkel_mhs' => $this->input->post('jenkel'),
				'ftgllahir_mhs' => $this->input->post('txt_tgl_lahir'),
				'fpasswd_mhs' => $this->input->post('txt_password')
			];
			
			$this->db->update('tbl_mhs', $data, ['fnim_mhs' => $fnim_mhs]);
			
			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $fnim_mhs . ' berhasil diubah !!!</div>');
			}
		
			redirect('mhs');
		}
	}

	public function ubah($fnim_mhs)
	{
		$query = $this->db->get_where('tbl_mhs', ['fnim_mhs'=>$fnim_mhs]);
		
		if ($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				$this->data['data_mhs_edit'] = $row;
			}
		}
		else
		{
			$this->data['data_mhs_edit'] = NULL;
		}

		$this->data['_content'] = 'v_mhs/ubah_mhs_view';

		$this->load->view('themes/main', $this->data);
	}

	public function hapus($fnim_mhs)
	{
		$query = $this->db->delete('tbl_mhs', ['fnim_mhs'=>$fnim_mhs]);

		redirect('mhs');
	}

}
