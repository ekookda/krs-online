<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{
	
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != 'login')
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		$data['_content']='Home-new';
		$this->load->view('themes/main', $data);
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}

}
