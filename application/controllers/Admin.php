<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Project Aplikasi
 * Kartu Rencana Studi 
 */
class Admin extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('role') != 1 || $this->session->userdata('status') != 'login')
		{
			redirect('login');
		}
	}

	public function index()
	{
		$data = [
			'dt_admin' => $this->db->get('tbl_admin')->result_array(),
			'_content' => 'v_admin/admin_view'
		];

		$this->load->view('themes/main', $data);
	}
	
	public function tambah()
	{
		$this->data['_content'] = 'v_admin/add_admin_view';

		$this->load->view('themes/main', $this->data);
	}

	public function simpan($data=NULL)
	{
		if ($this->input->post('btn_simpan'))
		{
			$data = [
				'fnip'    => $this->input->post('txt_nip_admin'),
				'fnama'   => ucwords($this->input->post('txt_nama_admin')),
				'fpasswd' => $this->input->post('txt_pwd'),
				'frole'   => $this->input->post('txt_role')
			];

			$this->db->insert('tbl_admin', $data);
			
			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $kriteria['txt_nip_admin_edit'] . ' berhasil disimpan !!!</div>');
			}
			
			redirect('admin');
		} 
		elseif ($this->input->post('btn_update')) 
		{
			$fnip = $this->input->post('txt_nip_admin_edit');

			$data = [
				'fnip' => $this->input->post('txt_nip_admin'),
				'fnama' => $this->input->post('txt_nama_admin'),
				'fpasswd' => $this->input->post('txt_pwd'),
				'frole' => $this->input->post('txt_role')
			];
			
			$this->db->update('tbl_admin', $data, ['fnip' => $fnip]);
			
			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $fnip . ' berhasil diubah !!!</div>');
			}
			
			redirect('admin');
		}
	}

	public function ubah($fnip)
	{
		$query = $this->db->get_where('tbl_admin', ['fnip'=>$fnip]);
		
		if ($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				$this->data['data_admin_edit'] = $row;
			}
		}
		else
		{
			$this->data['data_admin_edit'] = NULL;
		}
		$this->data['_content'] = 'v_admin/ubah_admin_view';

		$this->load->view('themes/main', $this->data);
	}

	public function hapus($fnip)
	{
		$this->db->delete('tbl_admin', ['fnip'=>$fnip]);

		redirect('admin');
	}

}
