<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Project Aplikasi
 * Kartu Rencana Studi 
 */
class Matkul extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
	}

	public function index()
	{
		$data = [
			'dt_matkul' => $this->db->get('tbl_matkul')->result_array(),
			'_content' => 'v_matkul/matkul_view'
		];

		$this->load->view('themes/main', $data);
	}
	
	public function tambah()
	{
		$this->data['_content'] = 'v_matkul/add_matkul_view';

		$this->load->view('themes/main', $this->data);
	}

	public function simpan($id = NULL)
	{
		if ($this->input->post('btn_simpan'))
		{
			$data = [
				'fkd_matkul'        => $this->input->post('txt_kd_matkul'),
				'fnm_matkul'        => $this->input->post('txt_nama_matkul'),
				'fjml_sks'          => $this->input->post('txt_jml_sks'),
				'fjenis_matkul'     => $this->input->post('txt_jenis_matkul'),
				'fsingkatan_matkul' => $this->input->post('txt_singkatan_matkul')
			];
			
			$this->db->insert('tbl_matkul', $data);

			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $kriteria['txt_nip_admin_edit'] . ' berhasil disimpan !!!</div>');
			}
			redirect('matkul');
		} 
		elseif ($this->input->post('btn_update')) 
		{
			$fkd_matkul = $this->input->post('txt_kd_matkul_edit');
			$data = [
				'fkd_matkul' => $this->input->post('txt_kd_matkul'),
				'fnm_matkul' => $this->input->post('txt_nama_matkul'),
				'fjml_sks' => $this->input->post('txt_jml_sks'),
				'fjenis_matkul' => $this->input->post('txt_jenis_matkul'),
				'fsingkatan_matkul' => $this->input->post('txt_singkatan_matkul')
			];
			
			$this->db->update('tbl_matkul', $data, ['fkd_matkul' => $fkd_matkul]);

			if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal diubah !!!</div>');
            }
            else
            {
				$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $fkd_matkul . ' berhasil diubah !!!</div>');
			}

			redirect('matkul');
		}
	}

	public function ubah($fkd_matkul)
	{
		$query = $this->db->get_where('tbl_matkul', ['fkd_matkul'=>$fkd_matkul]);
		
		if ($query->num_rows() == 1)
		{
			foreach ($query->result_array() as $row)
			{
				$this->data['data_matkul_edit'] = $row;
			}
		}
		else
		{
			$this->data['data_matkul_edit'] = NULL;
		}
		$this->data['_content'] = 'v_matkul/ubah_matkul_view';

		$this->load->view('themes/main', $this->data);
	}

	public function hapus($fkd_matkul)
	{
		$delete = $this->db->delete('tbl_matkul', ['fkd_matkul'=>$fkd_matkul]);
		if ($delete) 
		{
			$this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data berhasil dihapus !!!</div>');
		}
		redirect('matkul');
	}

}
