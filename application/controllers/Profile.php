<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller 
{

    public function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('status') != 'login') redirect('login');
    }

    public function index()
    {
        if ($this->input->post('btn-simpan'))
        {
            $data_kirim = [
                'fnama' => $this->input->post('txt_nama_admin'),
                'fpasswd' => $this->input->post('txt_password')
            ];
            $kriteria = ['fnip' => $this->input->post('txt_nip_admin_edit')];
            $this->db->update('tbl_admin', $data_kirim, $kriteria);

            if ($this->db->affected_rows() != 1)
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">' . $this->db->last_query() . ' Data gagal disimpan !!!</div>');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-success text-center">Data ' . $kriteria['txt_nip_admin_edit'] . ' Data gagal disimpan !!!</div>');
            }
            redirect('home');
        }
        else
        {
            $query = $this->db->get_where('tbl_admin', ['fnip'=>$this->session->userdata('user_id')]);
            $this->data['data_profile'] = $query->row_array();
            $this->data['_content'] = 'v_profile/update_profile_view';
            
            $this->load->view('themes/main', $this->data);
        }
    }

}

/* End of file Profile.php */