<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // if ($this->session->userdata('status') != 'login') redirect('login');
    }

    public function index()
    {
        if ($this->session->userdata('status') == "login") redirect('home');
        
        if (isset($_POST['btn-login']))
        {
            $user_name = $this->input->post('txt_user_name');
            $password  = $this->input->post('txt_password');
            $query     = $this->db->get_where('tbl_admin', ['fnip' => $user_name, 'fpasswd' => $password]);
            $dt_login  = $query->row_array();

            if (isset($dt_login))
            {
                $data_session = [
                    'status'    => 'login',
                    'user_id'   => $dt_login['fnip'],
                    'nama_user' => $dt_login['fnama'],
                    'role'      => $dt_login['frole']
                ];
                $this->session->set_userdata($data_session);
                redirect('home');
            }
            else
            {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">User name atau Password Salah !!!</div>');
            }
        }
        $this->load->view('v_login');
    }

}

/* End of file filename.php */