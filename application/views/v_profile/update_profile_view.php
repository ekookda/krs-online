<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome, <?=$this->session->userdata('nama_user');?>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url('profile');?>">Profile</a></li>
			<li class="active">Update Profile</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Update Profile</h3>
					</div>
					<!-- /.box-header -->
					
					<!-- form start -->
					<?php
					$attribut_text = ['class'=>'form-control'];
					$attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
					echo form_open('', $attribut_form);
					?>
					<div class="box-body">
						<div class="form-group">
							<label for="txt_nip_admin" class="col-sm-2 control-label">Nomor Induk Pegawai</label>
							<div class="col-sm-10">
								<?= form_input('txt_nip_admin', $data_profile['fnip'], ['class'=>'form-control', 'readonly'=>'readonly']); ?>
								<?= form_hidden('txt_nip_admin_edit', $data_profile['fnip'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_nama_admin" class="col-sm-2 control-label">Nama Admin</label>
							<div class="col-sm-10">
								<?= form_input('txt_nama_admin', $data_profile['fnama'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_pwd" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-10">
								<?= form_password('txt_pwd', $data_profile['fpasswd'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_role" class="col-sm-2 control-label">Role</label>
							<div class="col-sm-10">
								<?php
									$role_admin = ['99'=>'Pilih Role', '0'=>'User', '1'=>'Admin'];
									echo form_dropdown('txt_role', $role_admin, $data_profile['frole'], ['class'=>'form-control', 'disabled'=>'disabled']);
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="tags" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-10">
								<?= form_submit('btn_simpan', 'Submit', ['class'=>'btn btn-primary btn-flat']); ?>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div  class="box-footer">
						<a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
					</div>
					<!-- /. box-footer -->
				<?= form_close(); ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
