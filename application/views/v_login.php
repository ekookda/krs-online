<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
    <title>KRS Online | Log in</title>
    <link rel="stylesheet" href="<?=base_url('assets/bootstrap/dist/css/bootstrap.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/font-awesome/css/font-awesome.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/Ionicons/css/ionicons.min.css');?>">
    <link rel="stylesheet" href="<?=base_url('assets/dist/css/AdminLTE.min.css');?>">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="<?=site_url('home');?>"><b>KRS</b> Online</a>
	</div>
	
	<!-- /.login-logo -->
	<div class="login-box-body">
        <?php if ($this->session->flashdata('msg')) echo $this->session->flashdata('msg'); ?>
		<p class="login-box-msg text-center">Silahkan Login</p>

        <?php         
        $attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
        echo form_open('', $attribut_form); ?>

			<div class="form-group has-feedback">
                <?php
                $attr_array = ['class'=>'form-control', 'placeholder'=>'User name', 'autocomplete'=>'off', 'required'=>'required'];
                echo form_input('txt_user_name', '', $attr_array);
                ?>
				<span class="glyphicon glyphicon-user form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
            <?php
                $attr_array = ['class'=>'form-control', 'placeholder'=>'Password', 'required'=>'required'];
                echo form_password('txt_password', '', $attr_array);
            ?>
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">		
                <div class="col-xs-12">
                    <button type="submit" name="btn-login" id="btn-login" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Sign In</button>
                </div>
                <!-- /.col -->
			</div>

		<?php echo form_close(); ?>

	</div> <!-- /.login-box-body -->
</div> <!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?=base_url('assets/jquery/dist/jquery.min.js');?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url('assets/bootstrap/dist/js/bootstrap.min.js');?>"></script>

</body>
</html>
