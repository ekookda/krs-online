<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome, <?=$this->session->userdata('nama_user');?>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url('matkul');?>">Mata Kuliah</a></li>
			<li class="active">Ubah Mata Kuliah</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data Matakuliah</h3>
					</div>
					<!-- /.box-header -->
					
					<!-- form start -->
					<?php
					$attribut_text = ['class'=>'form-control'];
					$attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
					echo form_open('matkul/simpan/'.$data_matkul_edit['fkd_matkul'], $attribut_form);
					?>
					<div class="box-body">

						<div class="form-group">
							<label for="txt_kd_matkul" class="col-sm-2 control-label">Kode Mata Kuliah</label>
							<div class="col-sm-10">
								<?= form_input('txt_kd_matkul', $data_matkul_edit['fkd_matkul'], $attribut_text); ?>
								<?= form_hidden('txt_kd_matkul_edit', $data_matkul_edit['fkd_matkul'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_nama_matkul" class="col-sm-2 control-label">Nama Matakuliah</label>
							<div class="col-sm-10">
								<?= form_input('txt_nama_matkul', $data_matkul_edit['fnm_matkul'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_jml_sks" class="col-sm-2 control-label">Jumlah SKS</label>
							<div class="col-sm-10">
								<?= form_input('txt_jml_sks', $data_matkul_edit['fjml_sks'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_jenis_matkul" class="col-sm-2 control-label">Jenis Mata Kuliah</label>
							<div class="col-sm-10">
								<?php
									$jenis_matkul = [
										'99' => 'Pilih Jenis Matakuliah',
										'Inti' => 'Inti',
										'Wajib' => 'Wajib',
										'Pilihan' => 'Pilihan'
									];
									echo form_dropdown('txt_jenis_matkul', $jenis_matkul, $data_matkul_edit['fjenis_matkul'], $attribut_text);
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_singkatan_matkul" class="col-sm-2 control-label">Nama Singkatan Mata Kuliah</label>
							<div class="col-sm-10">
								<?= form_input('txt_singkatan_matkul', $data_matkul_edit['fsingkatan_matkul'], $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="tags" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-10">
								<?= form_submit('btn_update', 'Submit', ['class'=>'btn btn-primary btn-flat']); ?>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div  class="box-footer">
						<a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
					</div>
					<!-- /. box-footer -->
					<?= form_close(); ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
