<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome,
			<small><?=$this->session->userdata('nama_user');?></small>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Matakuliah</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?= $this->session->flashdata('msg');?>

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data Matakuliah</h3>
					</div>
					<!-- /.box-header -->
					
					<div class="box-body table-responsive">
						<div class="form-group">
							<a href="<?=site_url('matkul/tambah');?>" class="btn btn-primary btn-flat">
								<i class="fa fa-plus"></i> Tambah</a>
						</div>
						<table class="table table-bordered table-striped" id="example2">	
							<thead>
							<tr>
								<th>No</th>
								<th>Kode Matkul</th>
								<th>Nama Matkul</th>
								<th>SKS</th>
								<th>Jenis Matakuliah</th>
								<th>Singkatan</th>
								<th width="100">Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
								$nomor = 0;
								foreach ($dt_matkul as $data_record) {
									$nomor++;
							?>
									<tr>
										<td><?=$nomor;?></td>
										<td><?=$data_record['fkd_matkul'];?></td>
										<td><?=$data_record['fnm_matkul'];?></td>
										<td><?=$data_record['fjml_sks'];?></td>
										<td><?=$data_record['fjenis_matkul'];?></td>
										<td><?=$data_record['fsingkatan_matkul'];?></td>
										<td align="center">
											<?= anchor('matkul/ubah/' . $data_record['fkd_matkul'], '<i class="fa fa-pencil"></i>', ['class'=>'btn btn-primary btn-flat']); ?>
											<?= anchor('matkul/hapus/' . $data_record['fkd_matkul'], '<i class="fa fa-trash"></i>', ['class'=>'btn btn-warning btn-flat']); ?>
										</td>
									</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.table-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
