<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome,
			<small><?=$this->session->userdata('nama_user');?></small>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Admin</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?= $this->session->flashdata('msg');?>

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data Admin</h3>
					</div>
					<!-- /.box-header -->
					
					<div class="box-body table-responsive">
						<div class="form-group">
							<a href="<?=site_url('admin/tambah');?>" class="btn btn-primary btn-flat">
								<i class="fa fa-plus"></i> Tambah</a>
						</div>
						<table class="table table-bordered table-striped" id="example2">	
							<thead>
							<tr>
								<th>No</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Password</th>
								<th>Role</th>
								<th width="100">Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
								$nomor = 0;
								foreach ($dt_admin as $data_record) {
									$nomor++;
							?>
									<tr>
										<td><?=$nomor;?></td>
										<td><?=$data_record['fnip'];?></td>
										<td><?=$data_record['fnama'];?></td>
										<td><?=$data_record['fpasswd'];?></td>
										<td><?=$data_record['frole'];?></td>
										<td align="center">
											<?= anchor('admin/ubah/' . $data_record['fnip'], '<i class="fa fa-pencil"></i>', array('class'=>'btn btn-primary btn-flat')); ?>
											<?= anchor('admin/hapus/' . $data_record['fnip'], '<i class="fa fa-trash"></i>', array('class'=>'btn btn-warning btn-flat')); ?>
										</td>
									</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.table-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
