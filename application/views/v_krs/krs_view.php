<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome,
			<small><?=$this->session->userdata('nama_user');?></small>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">K R S    </li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?= $this->session->flashdata('msg');?>

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data KRS</h3>
					</div>
					<!-- /.box-header -->
					
					<div class="box-body table-responsive">
						<div class="form-group">
							<a href="<?=site_url('krs_admin/tambah');?>" class="btn btn-primary btn-flat">
								<i class="fa fa-plus"></i> Tambah</a>
						</div>
						<table class="table table-hover table-striped table-condensed" id="example2">	
							<thead>
							<tr>
								<th class="text-center">No</th>
								<th class="text-center">Tahun Ajaran</th>
								<th class="text-center">Semester</th>
								<th class="text-center">NIM</th>
								<th class="text-center">Mata Kuliah</th>
								<th class="text-center">Kelompok</th>
								<th class="text-center">Date Log</th>
								<th class="text-center">User Log</th>
								<th width="100" class="text-center">Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
								$nomor = 0;
								foreach ($dt_krs as $data_record) {
									$nomor++;
							?>
									<tr>
										<td class="text-center"><?= $nomor; ?></td>
										<td class="text-center"><?= substr_replace($data_record['fthn_ajar'], '/', 4, -4); ?></td>
										<td class="text-center"><?= $data_record['fsmt']; ?></td>
										<td class="text-center"><?= $data_record['fnim_mhs']; ?></td>
										<td class="text-center"><?= $data_record['fkd_matkul']; ?></td>
										<td class="text-center"><?= $data_record['fkelompok']; ?></td>
										<td class="text-center"><?= date('d F Y', strtotime($data_record['ftlg_log'])); ?></td>
										<td class="text-center"><?= $data_record['fuserlog']; ?></td>
										<td  class="text-center">
											<?= anchor('krs_admin/ubah/' . $data_record['fnim_mhs'].'/'. $data_record['fkd_matkul'], '<i class="fa fa-pencil"></i>', ['class'=>'btn btn-primary btn-flat btn-xs']); ?>
											<?= anchor('krs_admin/hapus/' . $data_record['fnim_mhs'].'/'. $data_record['fkd_matkul'], '<i class="fa fa-trash"></i>', ['class'=>'btn btn-danger btn-flat btn-xs']); ?>
										</td>
									</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.table-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
