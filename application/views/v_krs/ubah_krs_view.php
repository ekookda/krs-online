<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome, <?=$this->session->userdata('nama_user');?>
		</h1>

		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url('krs_admin');?>">Kartu Rencana Studi</a></li>
			<li class="active">Tambah KRS</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data KRS</h3>
					</div>
					<!-- /.box-header -->

					<!-- form start -->
					<?php
					$attribut_text = ['class'=>'form-control'];
					$attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
					echo form_open('krs_admin/simpan', $attribut_form);
					?>
					<div class="box-body">

						<div class="form-group">
							<label for="txt_thn_ajar" class="col-sm-2 control-label">Tahun Ajaran</label>
							<div class="col-sm-10">
								<?php
									$year_awal = date('Y');
									echo "<select name='txt_thn_ajar' class='form-control'>";
									for ($i=($year_awal-1); $i <= ($year_awal+5); $i++)
									{
										echo "<option value='".$i.($i+1)."'>".$i."/".($i+1)."</option>";
									}
									echo "</select>";
								?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_smt" class="col-sm-2 control-label">Semester</label>
							<div class="col-sm-10">
								<select class="form-control" name="txt_smt">
								<?php
									for ($i=1; $i <= 8; $i++) 
									{
										if ($i%2 == 0)
										{
											$smt = 'GENAP';
										}
										else
										{
											$smt = 'GASAL';
										}
										echo "<option value='".$i."'>$i</option>";
									}
								?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_nim" class="col-sm-2 control-label">Nama Mahasiswa</label>
							<div class="col-sm-10">
								<select name="txt_nim" id="txt_nim" class="form-control">
									<?php
									foreach ($fnim as $mhs)
									{
										echo "<option value='".$mhs['fnim_mhs']."'>" . $mhs['fnim_mhs'] . " - " . strtoupper($mhs['fnama_mhs']). "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_matkul" class="col-sm-2 control-label">Mata Kuliah</label>
							<div class="col-sm-10">
								<select name="txt_matkul" id="txt_matkul" class="form-control">
									<?php
									foreach ($fmatkul as $matkul)
									{
										echo "<option value='".$matkul['fkd_matkul']."'>" . $matkul['fkd_matkul'] . " - " . strtoupper($matkul['fnm_matkul']). "</option>";
									}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_kelompok" class="col-sm-2 control-label">Kelompok</label>
							<div class="col-sm-10">
								<?= form_input('txt_kelompok', '', $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="tags" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-10">
								<?= form_submit('btn_simpan', 'Simpan', ['class'=>'btn btn-primary btn-flat']); ?>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div  class="box-footer">
						<a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
					</div>
					<!-- /. box-footer -->
				<?= form_close(); ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>