<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome, <?=$this->session->userdata('nama_user');?>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?=site_url('mhs');?>">Mahasiswa</a></li>
			<li class="active">Tambah Mahasiswa</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data Mahasiswa</h3>
					</div>
					<!-- /.box-header -->
					
					<!-- form start -->
					<?php
					$attribut_text = ['class'=>'form-control'];
					$attribut_form = ['class'=>'form-horizontal', 'id'=>'myform'];
					echo form_open('mhs/simpan', $attribut_form);
					?>
					<div class="box-body">

						<div class="form-group">
							<label for="txt_nim_mhs" class="col-sm-2 control-label">NIM</label>
							<div class="col-sm-10">
								<?= form_input('txt_nim_mhs', '', $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_nama_mhs" class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-10">
								<?= form_input('txt_nama_mhs', '', $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_no_ktp" class="col-sm-2 control-label">No KTP</label>
							<div class="col-sm-10">
								<?= form_input('txt_no_ktp', '', $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_no_ktp" class="col-sm-2 control-label">No KTP</label>
							<div class="col-sm-10">
								<?= form_input(['type'=>'date', 'name'=>'txt_tgl_lahir', 'class'=>'form-control', 'required'=>'required']); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_agama" class="col-sm-2 control-label">Agama</label>
							<div class="col-sm-10">
							<?php
								$agama = [
									'99' => 'Pilih Agama',
									'1' => 'Islam',
									'2' => 'Katolik',
									'3' => 'Protestan',
									'4' => 'Hindu',
									'5' => 'Budha'
								];
								echo form_dropdown('txt_agama', $agama, '99', $attribut_text);
							?>
							</div>
						</div>
						<div class="form-group">
							<label for="txt_jenkel" class="col-sm-2 control-label">Jenis Kelamin</label>
							<div class="col-sm-10">
								<input type="radio" name="jenkel" value="1"> Laki-laki
								<input type="radio" name="jenkel" value="2"> Perempuan
							</div>
						</div>
						<div class="form-group">
							<label for="txt_singkatan_mhs" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-10">
								<?= form_password('txt_password', '', $attribut_text); ?>
							</div>
						</div>
						<div class="form-group">
							<label for="tags" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-10">
								<?= form_submit('btn_simpan', 'Simpan', ['class'=>'btn btn-primary btn-flat']); ?>
							</div>
						</div>
					</div>
					<!-- /.box-body -->
					<div  class="box-footer">
						<a onclick="window.history.back(-1)" class="btn btn-success btn-flat"><i class="fa fa-step-backward"></i> Kembali</a>
					</div>
					<!-- /. box-footer -->
				<?= form_close(); ?>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
