<!-- Conten Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page Header) -->
	<section class="content-header">
		<h1>
			Welcome,
			<small><?=$this->session->userdata('nama_user');?></small>
		</h1>
	
		<ol class="breadcrumb">
			<li><a href="<?=site_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Mahasiswa</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?= $this->session->flashdata('msg');?>

				<!-- Horizontal Form -->
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data Mahasiswa</h3>
					</div>
					<!-- /.box-header -->
					
					<div class="box-body table-responsive">
						<div class="form-group">
							<a href="<?=site_url('mhs/tambah');?>" class="btn btn-primary btn-flat">
								<i class="fa fa-plus"></i> Tambah</a>
						</div>
						<table class="table table-bordered table-striped" id="example2">	
							<thead>
							<tr>
								<th>NIM</th>
								<th>Nama Mahasiswa</th>
								<th>No KTP</th>
								<th>Agama</th>
								<th>Jenis Kelamin</th>
								<th width="100">Action</th>
							</tr>
							</thead>
							<tbody>
							<?php
								$nomor = 0;
								foreach ($dt_mhs as $data_record) {
									$nomor++;
							?>
									<tr>
										<td><?=$data_record['fnim_mhs'];?></td>
										<td><?=$data_record['fnama_mhs'];?></td>
										<td><?=$data_record['fktp_mhs'];?></td>
										<td><?=$data_record['fagama_mhs'];?></td>
										<td><?=$data_record['fjenkel_mhs'];?></td>
										<td align="center">
											<?= anchor('mhs/ubah/' . $data_record['fnim_mhs'], '<i class="fa fa-pencil"></i>', ['class'=>'btn btn-primary btn-flat']); ?>
											<?= anchor('mhs/hapus/' . $data_record['fnim_mhs'], '<i class="fa fa-trash"></i>', ['class'=>'btn btn-warning btn-flat']); ?>
										</td>
									</tr>
							<?php } ?>
							</tbody>
						</table>
					</div>
					<!-- /.table-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col (right) -->
		</div>
		<!-- /.row -->
	</section>
</div>
